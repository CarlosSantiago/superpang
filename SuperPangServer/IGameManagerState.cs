﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer
{
    interface IGameManagerState
    {
        void Update();
        bool onEnter(GameManager manager);
        bool onExit();
    }
}
