﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperPangServer.SendPackets;
using System.Threading;
using SuperPangLib;

namespace SuperPangServer
{
    class InGameState : IGameManagerState
    {
        private GameManager manager;
        private bool gameRuning;

    

        public bool onEnter(GameManager manager)
        {
            this.gameRuning = false;
            this.manager = manager;
            Log.Info("g_id: " + manager.gameID + " OnEnter InGameState");

            foreach (Connection con in manager.connections)
            {
                con.player.init();
                con.MessagesToSend.Enqueue(new SendPLevelSetup(manager.connections, con.player.id));
            }

            
            //Thread worldStateThread = new Thread(sendWorldState);
            //worldStateThread.Start();
            //gameRuning = true;

            return true;
        }
        public void Update()
        {
            //PROVISIONAL: pierde mucha eficiencia cuando hay mas de dos jugadores en una instancia
            foreach (Connection con in manager.connections)
            {
                if (con.player.posX != con.player.lastSentPosX)
                {
                    foreach (Connection con2 in manager.connections)
                    {
                        if (con2.player.id == con.player.id)
                            continue;
                        con2.MessagesToSend.Enqueue(new SendPPlayerMoved(manager.connections, con2.player.id));
                        
                    }
                    con.player.lastSentPosX = con.player.posX;                    
                }
                if (con.player.shoot)
                {
                    foreach (Connection con2 in manager.connections)
                    {
                        if (con2.player.id == con.player.id)
                            continue;
                        con2.MessagesToSend.Enqueue(new SendPPlayerShoot(con));
                        
                    }
                    con.player.shoot = false;              
                    
                }
            }

        }
        public bool onExit()
        {
            
            return true;
        }

        //private void sendWorldState()
        //{
        //    while (true)
        //    {
        //        if (!gameRuning)
        //            continue;
        //        foreach (Connection con in manager.connections)
        //        {                    
        //            con.MessagesToSend.Enqueue(new SendPPlayerMoved(manager.connections, con.player.id));
        //        }
        //        Thread.Sleep(100);
        //    }
        //}

    }
}
