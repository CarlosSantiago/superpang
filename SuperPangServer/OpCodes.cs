﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperPangServer.RecvPackets;
using SuperPangServer.SendPackets;

namespace SuperPangServer
{
    public class OpCodes
    {
        public static Dictionary<int, Type> Recv = new Dictionary<int, Type>();
        public static Dictionary<Type, int> Send = new Dictionary<Type, int>();

        public static void Init()
        {
            Recv.Add(1, typeof(RecvPHay));
            Recv.Add(2, typeof(RecvPJoinGame));
            Recv.Add(5, typeof(RecvPPlayerPos));
            Recv.Add(6, typeof(RecvPPlayerShoot));

            Send.Add(typeof(SendPLevelSetup), 3);
            Send.Add(typeof(SendPPlayerMoved), 4);
            Send.Add(typeof(SendPPlayerShoot), 7);
        }
    }
}
