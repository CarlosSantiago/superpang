﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer.RecvPackets
{
    class RecvPJoinGame : IRecvPacket
    {
        public override void Process(Connection connection, List<string> data)
        {
            Object gameManagerLock = new Object();
            Log.Info("IP: " + connection.socket.RemoteEndPoint.ToString() + " identificado como: " + data[1] + " solicita partida con id: "+ data[0]);            
            int gameID;
            connection.player.username = data[1];            
            try
            {
                gameID = Convert.ToInt32(data[0]);
            }
            catch
            {                
                Log.Error("Error de formato en RecvJoinGame");
                return;
            }
            
            if (GameManager.runningGames.ContainsKey(gameID))
            {
                Log.Info("Partida " + data[0] + " ya existe. Añadiendo a " + data[1]);
                connection.player.id = GameManager.runningGames[gameID].connections.Count + 1;
                GameManager.runningGames[gameID].addConnection(connection);
            }
            else
            {
                Log.Info("Partida " + data[0] + " no existe. Creando GameManager y añadiendo a "+data[1]+" . MaxPlayers: "+ data[2]);                
                int maxPlayers = Convert.ToInt16(data[2]);
                connection.player.id = 1;
                GameManager.runningGames.Add(gameID, new GameManager(gameID, connection, maxPlayers));               
                
            }
        }
    }
}
