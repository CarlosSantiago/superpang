﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer.RecvPackets
{
    class RecvPPlayerPos : IRecvPacket
    {
        public override void Process(Connection connection, List<string> data)
        {
            //Log.Debug("Update position (" + connection.player.username + ") : x" + data[0] + " y" + data[1]);
            connection.player.posX = Convert.ToInt32(data[0]);
            connection.player.posY = Convert.ToInt32(data[1]);            
        }
    }
}
