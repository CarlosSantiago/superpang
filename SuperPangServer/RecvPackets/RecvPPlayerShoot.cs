﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer.RecvPackets
{
    class RecvPPlayerShoot : IRecvPacket
    {
        public override void Process(Connection connection, List<string> data)
        {
            Log.Debug("Player shoot ( id:" + connection.player.id + ") en: x" + data[0] + " y" + data[1]);
            connection.player.shoot = true;
            connection.player.lastShootPosX = Convert.ToInt32(data[0]);
            connection.player.lastShootPosY = Convert.ToInt32(data[1]);
        }
    }
}
