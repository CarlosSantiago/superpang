﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace SuperPangServer.RecvPackets
{
    abstract class IRecvPacket
    {
        public abstract void Process(Connection connection, List<string> data);       
    }
}
