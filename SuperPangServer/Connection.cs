﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using SuperPangServer.RecvPackets;
using SuperPangServer.SendPackets;


namespace SuperPangServer
{
    public class Connection
    {
        public Socket socket;
        public Player player;

        public Connection(Socket socket)
        {
            this.socket = socket;
            Connections.Add(this);
            this.player = new Player("unknown");
            Thread listener = new Thread(listen);
            listener.Start();
        }

        private void listen()
        {
            bool listening = true;

            while (listening)
            {               
                byte[] msg = new byte[80];
                try
                {
                    socket.Receive(msg);
                    string dirtyTrama = Encoding.ASCII.GetString(msg);
                    string trama = dirtyTrama.Trim('\0');                    
                    GameMessage mensaje = new GameMessage(trama);
                    if (mensaje.getOpCode() != 5)
                        Log.Debug("Trama recibida " + trama +" OPCODE = "+mensaje.getOpCode().ToString());

                    ((IRecvPacket)Activator.CreateInstance(OpCodes.Recv[mensaje.getOpCode()])).Process(this, mensaje.getData());                    
                 
                    
                }
                catch (SocketException ex)
                {
                    Log.ErrorException("SocketException", ex);
                    listening = false;
                }
                catch (Exception ex) 
                {
                    Log.ErrorException("Exception en el socket: ", ex);
                    listening = false;
                }
            }
        }

        public static List<Connection> Connections = new List<Connection>();
        public static Thread SendAllThread = new Thread(SendAll);

        public static void initSendAll()
        {
            SendAllThread.Start();
        }        

        private static void SendAll()
        {
            while (true)
            {
                for (int i = 0; i < Connections.Count; i++)
                {
                    try
                    {
                        if (!Connections[i].Send())
                        {
                            Connections.RemoveAt(i--);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorException("Connection: SendAll:", ex);
                    }
                }

                Thread.Sleep(10);
            }
        }

        public Queue<ISendPacket> MessagesToSend = new Queue<ISendPacket>();

        public bool Send()
        {
            if (MessagesToSend.Count < 1)
                return true;

            string trama = string.Empty;

            while (MessagesToSend.Count > 0)
            {
                trama += MessagesToSend.Dequeue().getTrama();
                trama += "%";
            }

            if (socket.Send(Encoding.ASCII.GetBytes(trama)) == 0)
            {
                return false;
            }            
            return true;
        }

    

    
    }
}
