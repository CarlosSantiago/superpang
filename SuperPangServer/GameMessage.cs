﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer
{
    public class GameMessage
    {
        private string asciiMessage;
        private int opCode;
        private List<string> data;

        public GameMessage(string asciiMessage)
        {
            this.asciiMessage = asciiMessage;

            string[] splited = asciiMessage.Split('-');
            this.opCode = Convert.ToInt16(splited[0]);
            this.data = new List<string>();
            for (int i = 1; i<splited.Length;i++)
            {
                data.Add(splited[i]);
            }
        }

        public int getOpCode()
        {
            return this.opCode;
        }
        public List<string> getData()
        {
            return this.data;
        }
       
    }
}
