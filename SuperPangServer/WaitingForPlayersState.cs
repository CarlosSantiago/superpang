﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer
{
    class WaitingForPlayersState : IGameManagerState
    {
        private GameManager manager;

        public bool onEnter(GameManager manager)
        {
            this.manager = manager;
            Log.Info("g_id: " + manager.gameID + " Iniciado WaitingForPlayers state ");
            return true;
        }
        public void Update()
        {
            if (manager.connections.Count == manager.maxPlayers)
            {
                Log.Info("g_id: " + manager.gameID + " Todos los jugadores conectados");
                this.manager.changeState(new InGameState());

            }
        }
        public bool onExit()
        {
            Log.Info("g_id: " + manager.gameID + " Saliendo WaitingForPlayers state ");
            return true;
        }
    }
}
