﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;

namespace SuperPangServer
{
    class Program
    {
        static void Main(string[] args)
        {            
            Log.Info("Punto de inicio");

            TcpServer server = new TcpServer("192.168.1.128", 5002);
            if (server.init())
            {
                Log.Info("Servidor iniciado correctamente ");
            }
            else
            {
                Log.Error("Error al iniciar el servidor");
            }

            Connection.initSendAll();
            Log.Info("SendAll iniciado correctamente");

            while (true)
            {
                for (int count = 0; count < GameManager.runningGames.Count; count++)
                {
                    var element = GameManager.runningGames.ElementAt(count);
                    element.Value.Update();                   
                }
              
            }

        }
    }
}
