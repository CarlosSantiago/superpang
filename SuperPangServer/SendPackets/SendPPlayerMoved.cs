﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer.SendPackets
{
    public class SendPPlayerMoved : ISendPacket
    {
        private List<Connection> connections;
        private int playerID;
        public SendPPlayerMoved(List<Connection> connections, int playerID)
        {
            this.connections = connections;
            this.playerID = playerID;
        }
        public string getTrama()
        {
            string trama = OpCodes.Send[this.GetType()] + "-" + (connections.Count - 1).ToString();
            foreach (Connection con in connections)
            {
                if (con.player.id == playerID)
                    continue;

                trama += "-" + con.player.posX + "-" + con.player.posY;
                //Log.Debug("Enviando PlayerMoved (" + con.player.username + "): " + trama);                
            }
            return trama;
        }
    }
}
