﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer.SendPackets
{
    class SendPLevelSetup : ISendPacket
    {
        private List<Connection> connections;
        private int playerID;
        public SendPLevelSetup(List<Connection> connections, int playerID)
        {
            this.connections = connections;
            this.playerID = playerID;
        }
        public string getTrama()
        {
            string trama = OpCodes.Send[this.GetType()] + "-" + playerID.ToString() + "-" + connections.Count.ToString();
            foreach (Connection con in connections)
            {
                trama += "-" + con.player.posX + "-" + con.player.posY;
            }
            Log.Info("Trama gameSetup: " + trama);
            return trama;
        }
    
    
    }
}
