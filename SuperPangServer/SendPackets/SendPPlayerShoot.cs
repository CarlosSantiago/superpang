﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer.SendPackets
{
    class SendPPlayerShoot : ISendPacket
    {
        private Connection connection;
       
        public SendPPlayerShoot(Connection connection)
        {
            this.connection = connection;
            
        }
        public string getTrama()
        {
            string trama = OpCodes.Send[this.GetType()] + "-" + connection.player.id + "-" + connection.player.lastShootPosX + "-" + connection.player.lastShootPosY;
            Log.Debug("SendPlayerShoot: " + trama);
            return trama;
        }
    }
}
