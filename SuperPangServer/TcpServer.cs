﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace SuperPangServer
{
    class TcpServer
    {
        Socket listener;
        IPAddress direc;
        IPEndPoint ipep;

        public TcpServer(string ipAdress, int port)
        {
            direc = IPAddress.Parse(ipAdress);
            ipep = new IPEndPoint(direc, port);
        }

        public bool init()
        {
            OpCodes.Init();
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                listener.Bind(ipep);
            }
            catch (SocketException ex)
            {
                Log.ErrorException("Error al hacer el bind ", ex);
                return false;
            }
            try
            {
                listener.Listen(15);
            }
            catch (SocketException ex)
            {
                Log.ErrorException("Error al empezar el listener ", ex);
                return false;
            }

            
            Thread ThListener = new Thread(listen);
            ThListener.Start();
            return true;
        }

        private void listen()
        {
            bool listening = true;

            while (listening)
            {
                try
                {
                    Socket newClient = listener.Accept();

                    Log.Info("Nuevo cliente conectado: "+newClient.RemoteEndPoint.ToString());
                    new Connection(newClient);

                }
                catch (SocketException ex)
                {
                    Log.ErrorException("SocketException", ex);
                    listening = false;
                }
                catch (Exception ex)
                {
                    Log.ErrorException("Exception en el socket: ", ex);
                    listening = false;
                }
            }
        }
    }
}
