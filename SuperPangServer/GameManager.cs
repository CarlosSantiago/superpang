﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPangServer
{
    class GameManager
    {
        
        public static Dictionary<int, GameManager> runningGames = new Dictionary<int, GameManager>();

        public int gameID;
        public List<Connection> connections = new List<Connection>();
        public int maxPlayers;
        private Stack<IGameManagerState> states = new Stack<IGameManagerState>();

        public GameManager(int gameID, Connection connection, int maxPlayers)
        {
            this.gameID = gameID;
            this.connections.Add(connection);
            this.maxPlayers = maxPlayers;
            Log.Info("Nuevo GameManager creado por "+connection.player.username+" con id: " + gameID + " - Pushing WaitingForPlayers state");            
            this.states.Push(new WaitingForPlayersState());                      
            this.states.Peek().onEnter(this);
        }

        public bool changeState(IGameManagerState newState)
        {
            Log.Info("g_id: " + gameID + " Changing state...");
            
            if (!states.Pop().onExit())
                return false;
            states.Push(newState);
            if (!states.Peek().onEnter(this))
                return false;
            return true;
        }

        public void Update()
        {
            states.Peek().Update();
        }

        public bool addConnection(Connection newCon)
        {
            if (connections.Count >= maxPlayers)
                return false;
            connections.Add(newCon);
            Log.Info("g_id: " + gameID + " Player añadido: "+ newCon.player.username);
            return true;
        }
    }
}
