﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using System.Net;
using SuperPang.Networking;
using SuperPang.Networking.SendPackets;
using Microsoft.Xna.Framework;

namespace SuperPang
{
    class ConnectingState : IGameState
    {

        public bool onEnter()
        {
            Log.Info("Entrando en ConnectingState");

            OpCodes.Init();

            TcpClient client = new TcpClient(GlobalVars.serverAdress, GlobalVars.serverPort);
            if (client.init())
            {
                Log.Info("Conexión con el servidor realizada! ");
            }
            else
            {
                Log.Error("Error al conectar con el servidor ");
            }

            new SendPJoinGame(3, "carlos", 2).Send(TcpClient.client);
            return true;
        }

        public bool onExit()
        {
            Log.Info("Saliendo de ConnectingState");
            return true;
        }

        public void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                GameStateMachine.pushState(new PlayState());
            }
                
        }

        public void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            string drawString = "Esperando a jugadores";
            spriteBatch.DrawString(MyContentManager.arial, drawString, new Vector2((Game1.graphics.PreferredBackBufferWidth/2)-(MyContentManager.arial.MeasureString(drawString).X/2), 100), Color.White);
        }
    }
}
