﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPang
{
    abstract class GameState
    {
        public abstract void update();
        public abstract void render();
        public abstract bool onEnter();
        public abstract bool onExit();
    }
}
