﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperPang
{
    abstract class GameObject
    {
        protected Vector2 position;
        protected Vector2 dimensions;

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);
        public virtual Rectangle getCollisionRect()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)dimensions.X, (int)dimensions.Y);
        }
        public virtual Vector2 getCurrentPosition()
        {
            return new Vector2(position.X, position.Y);
        }
        public virtual Vector2 getDimensions()
        {
            return new Vector2(dimensions.X, position.Y);
        }
    }
}
