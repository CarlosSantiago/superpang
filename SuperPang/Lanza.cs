﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperPang
{
    class Lanza : GameObject
    {
        int lanzaOffsetX = 20;
        int lanzaOffsetY = 80;
        string textureName;
        int timeSinceLastUpdate;
        int timeSinceLastFrame;

        public Lanza(Vector2 inpos)
        {
            this.position.X = inpos.X + lanzaOffsetX;
            this.position.Y = inpos.Y + lanzaOffsetY;
            this.dimensions.X = 19;
            this.dimensions.Y = 0;
            textureName = "disparo";
        }
        public override void Update(GameTime gameTime)
        {
            timeSinceLastFrame = gameTime.ElapsedGameTime.Milliseconds;
            timeSinceLastUpdate += timeSinceLastFrame;

            position.Y -= (int)(0.45f * timeSinceLastFrame);
            dimensions.Y += (int)(0.45f * timeSinceLastFrame);            

            if (timeSinceLastUpdate > 60)
            {
                if (textureName == "disparo")
                    textureName = "disparo2";
                else if (textureName == "disparo2")
                    textureName = "disparo";
                timeSinceLastUpdate = 0;
            }
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            Texture2D texture = (Texture2D)typeof(MyContentManager).GetField(textureName).GetValue(typeof(MyContentManager));
            spriteBatch.Draw(texture, position, new Rectangle(0, 0, (int)dimensions.X, (int)dimensions.Y), Color.White);
        }
    }
}
