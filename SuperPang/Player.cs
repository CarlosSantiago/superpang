﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace SuperPang
{
    class Player
    {
        private Vector2 position;
        private Vector2 dimensions;

        private Dictionary<int, string> playerTextures = new Dictionary<int, string>();
        private int currentTextureKey;

        private int millisecondsPerAnimationFrame;
        private int milShotAniationLock;
        private int timeSinceLastFrame;

        private int shootsInSecond;
        private int shootsPerSecond;
        private bool shoot;
        private Vector2 shootPos;

        private bool walking;

        enum PlayerInputs { Right=1, Left, Shoot };

        public Vector2 getCurrentPosition()
        {
            return new Vector2(position.X, position.Y);
        }

        public Player(Vector2 startPosition)
        {
            playerTextures.Add(1, "mm0");
            playerTextures.Add(2, "mm1");
            playerTextures.Add(3, "mm2");
            playerTextures.Add(4, "mm3");
            playerTextures.Add(-1, "m0");
            playerTextures.Add(-2, "m1");
            playerTextures.Add(-3, "m2");
            playerTextures.Add(-4, "m3");
            playerTextures.Add(5, "mm4");
            playerTextures.Add(-5, "m4");
            playerTextures.Add(6, "mm5");
            playerTextures.Add(-6, "m5");

            position.X = startPosition.X;
            position.Y = startPosition.Y; 
            dimensions.X = 60;
            dimensions.Y = 78;
            currentTextureKey = 1;
            shootsPerSecond = 2;
            millisecondsPerAnimationFrame = 100;
            walking = false;
        }

        int animationTimer;
        int shootAnimationTimer;
        
        public void Update(GameTime gameTime)
        {
            animationTimer += gameTime.ElapsedGameTime.Milliseconds;
            shootAnimationTimer += gameTime.ElapsedGameTime.Milliseconds;
            timeSinceLastFrame = gameTime.ElapsedGameTime.Milliseconds;

            PlayerInputs userInput = getInput();

            if (shootAnimationTimer > milShotAniationLock)
            {
                if (userInput == PlayerInputs.Right)
                {
                    if(position.X < 720 )
                        position.X += (int)(0.35f * timeSinceLastFrame);
                }
                else if (userInput == PlayerInputs.Left)
                {
                    if(position.X > 20)
                        position.X -= (int)(0.35f * timeSinceLastFrame);
                }

                if (animationTimer > millisecondsPerAnimationFrame && (userInput == PlayerInputs.Left
                    || userInput == PlayerInputs.Right))
                {
                    walking = true;
                    if (userInput == PlayerInputs.Right)
                    {
                        if (currentTextureKey < 0)
                        {
                            currentTextureKey = 1;
                        }
                        else if (currentTextureKey > 3)
                        {
                            currentTextureKey = 1;
                        }
                        else
                        {
                            currentTextureKey++;
                        }
                    }
                    else if (userInput == PlayerInputs.Left)
                    {
                        if (currentTextureKey > 0)
                        {
                            currentTextureKey = -1;
                        }
                        else if (currentTextureKey < -3)
                        {
                            currentTextureKey = -1;
                        }
                        else
                        {
                            currentTextureKey--;
                        }
                    }
                    animationTimer = 0;
                }

                if (userInput == 0)
                {
                    walking = false;
                    if (currentTextureKey > 0)
                    {
                        currentTextureKey = 5;
                    }
                    else if (currentTextureKey < 0)
                    {
                        currentTextureKey = -5;
                    }
                }
            }

            if (userInput == PlayerInputs.Shoot)
            {
                if (shootsInSecond < shootsPerSecond)
                {
                    if (Math.Abs(currentTextureKey) != 6)
                    {
                        shoot = true;
                        if (currentTextureKey > 0) { shootPos = new Vector2(position.X + 19, position.Y + 78); }
                        if (currentTextureKey < 0) { shootPos = new Vector2(position.X + 15, position.Y + 78); }
                        shootsInSecond++;
                        shootAnimationTimer = 0;
                        MyContentManager.lanzar.Play();
                    }

                    if (currentTextureKey > 0)
                    {
                        currentTextureKey = 6;
                    }
                    if (currentTextureKey < 0)
                    {
                        currentTextureKey = -6;
                    }
                }
            }

            if (shootAnimationTimer > 1000)
            {
                shootsInSecond = 0;
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {            
            Texture2D texture = (Texture2D)typeof(MyContentManager).GetField(playerTextures[currentTextureKey]).GetValue(typeof(MyContentManager));
            spriteBatch.Draw(texture, position, Color.White);
        }

        private PlayerInputs getInput()
        {
            PlayerInputs input = 0;
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                input = PlayerInputs.Right;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                input = PlayerInputs.Left;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.LeftControl))
            {
                input = PlayerInputs.Shoot;
            }
            return input;
        }

        public Rectangle getCollisionRect()
        {
            return new Rectangle((int)position.X, (int)position.Y, (int)dimensions.X, (int)dimensions.Y);
        }

        public bool canShoot
        {
            get { return shoot; }
            set { shoot = value; }
        }
        public bool isWalking()
        {
            return this.walking;
        }

    }
}
