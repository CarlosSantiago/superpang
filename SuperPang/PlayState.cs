﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperPang
{
    class PlayState : IGameState
    {
        private UserInterface myInterface;
        private Player player;

        private List<GameObject> lanzas = new List<GameObject>();
        private List<GameObject> bolas = new List<GameObject>();

        private enum State { live, dead, win, lose };
        private State currentState;

        public bool onEnter()
        {
            Log.Info("Entrando PlayState");
            myInterface = new UserInterface();
            player = new Player(new Vector2(390, 420));
            currentState = State.live;
            bolas.Add(new BouncingBall(new Vector2(100,100), new Vector2(100,100), new Vector2(2,0)));
            return true;
        }
        public bool onExit()
        {
            Log.Info("Saliendo PlayState");
            return true;

        }
        public void Update(GameTime gameTime)
        {
            if (currentState == State.live)
            {
                player.Update(gameTime);
                if (player.canShoot)
                {                    
                    lanzas.Add(new Lanza(player.getCurrentPosition()));
                    player.canShoot = false;
                }
                
            }
            
            for (int x = 0; x < lanzas.Count; x++)
            {
                if (lanzas[x].getCurrentPosition().Y < 10)
                {
                    lanzas.RemoveAt(x);                    
                    break;
                }
                else
                    lanzas[x].Update(gameTime);
                for (int j = 0; j < bolas.Count; j++)
                {
                    if (lanzas[x].getCollisionRect().Intersects(bolas[j].getCollisionRect()))
                    {
                        switch((int)bolas[j].getDimensions().X)
                        {
                            case 100:
                                bolas.Add(new BouncingBall(bolas[j].getCurrentPosition(), new Vector2(50, 50), new Vector2(-2, -2)));
                                bolas.Add(new BouncingBall(bolas[j].getCurrentPosition(), new Vector2(50, 50), new Vector2(2,-2)));
                                break;
                            case 50:
                                bolas.Add(new BouncingBall(bolas[j].getCurrentPosition(), new Vector2(25, 25), new Vector2(-2, -2)));
                                bolas.Add(new BouncingBall(bolas[j].getCurrentPosition(), new Vector2(25, 25), new Vector2(2, -2)));
                                break;
                            case 25:
                                
                                break;
                        }
                        bolas.RemoveAt(j);
                        lanzas.RemoveAt(x);
                        MyContentManager.explotar.Play();
                        break;
                    }
                }
            }
            foreach (GameObject b in bolas)
            {
                b.Update(gameTime);
            }
            myInterface.Update(gameTime);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            myInterface.Draw(spriteBatch);
            foreach (GameObject o in lanzas)
            {
                o.Draw(spriteBatch);
            }
            foreach (GameObject b in bolas)
            {
                b.Draw(spriteBatch);
            }
            player.Draw(spriteBatch);            
            
        }
    }
}
