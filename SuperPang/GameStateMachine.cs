﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperPang
{
    interface IGameState
    {
        bool onEnter();
        bool onExit();
        void Update(GameTime gameTime);
        void Draw(SpriteBatch spriteBatch);
    }

    class GameStateMachine
    {
        private static Stack<IGameState> gameStates = new Stack<IGameState>();

        public static void pushState(IGameState pushState) 
        {
            Log.Info("StateMachine push: " + pushState.GetType().ToString());
            gameStates.Push(pushState);
            gameStates.Peek().onEnter();
        }
        public static void popState()
        {
            Log.Info("StateMachine popState");
            if (gameStates.Count > 0)
            {
                if (gameStates.Peek().onExit())
                {
                    gameStates.Pop();
                }
            }
        }
        public static void Update(GameTime gameTime) 
        {
            if (gameStates.Count > 0)
            {
                gameStates.Peek().Update(gameTime);
            }
        }
        public static void Draw(SpriteBatch spriteBatch) 
        {
            if (gameStates.Count > 0)
            {
                gameStates.Peek().Draw(spriteBatch);
            }
        }

    }
}
