﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SuperPang
{    
    class BouncingBall : GameObject
    {
        private int bouncingTime;        
        private int maxHeight;
        private Vector2 velocidad;
        private Vector2 inVelocidad;
        

        public BouncingBall(Vector2 ipos, Vector2 dimensions, Vector2 inVelocidad)
        {
            this.position.X = ipos.X;
            this.position.Y = ipos.Y;
            this.dimensions.X = dimensions.X;
            this.dimensions.Y = dimensions.Y;           
            this.inVelocidad = inVelocidad;
            this.velocidad.X += inVelocidad.X;
        }

        public override void Update(GameTime gameTime)
        {           
            bouncingTime += gameTime.ElapsedGameTime.Milliseconds;
          
            velocidad.Y = Convert.ToInt32(inVelocidad.Y + (7 * bouncingTime / 1000));

            position.Y += velocidad.Y;
            if (position.Y > 498 - dimensions.X)
            {
                inVelocidad.Y = -velocidad.Y;
                bouncingTime = 50;
            }
            if (position.X > 781 - dimensions.X || position.X < 18)
            {
                velocidad.X = -velocidad.X;
            }
            position.X += velocidad.X;
            
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            Texture2D texture = (Texture2D)typeof(MyContentManager).GetField("b"+dimensions.X.ToString()).GetValue(typeof(MyContentManager));
            spriteBatch.Draw(texture, position, new Rectangle(0, 0, (int)dimensions.X, (int)dimensions.Y), Color.White);
        }

      
    }
}
