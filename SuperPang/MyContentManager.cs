﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace SuperPang
{
    public class MyContentManager
    {
        static ContentManager manager;

        //Interfaz
        public static Texture2D bordes1;
        public static Texture2D backtest;

        //Player
        public static Texture2D m0;
        public static Texture2D m1;
        public static Texture2D m2;
        public static Texture2D m3;
        public static Texture2D m4;
        public static Texture2D m5;        
        public static Texture2D mm0;
        public static Texture2D mm1;
        public static Texture2D mm2;
        public static Texture2D mm3;
        public static Texture2D mm4;
        public static Texture2D mm5;    
 
        //Lanza
        public static Texture2D disparo;
        public static Texture2D disparo2;

        //Bolas
        public static Texture2D b25;
        public static Texture2D b50;
        public static Texture2D b100;

        //Efectos
        public static SoundEffect lanzar;
        public static SoundEffect explotar;        

        //Fuentes
        public static SpriteFont arial;
        public static SpriteFont miramob;
        

        public static void setContentM(ContentManager cmanager)
        {
            manager = cmanager;
        }

        public static bool LoadTextures()
        {
            try
            {
                bordes1 = manager.Load<Texture2D>("Interfaz/bordes1");
                m0 = manager.Load<Texture2D>("Player/m0");
                m1 = manager.Load<Texture2D>("Player/m1");
                m2 = manager.Load<Texture2D>("Player/m2");
                m3 = manager.Load<Texture2D>("Player/m3");
                m4 = manager.Load<Texture2D>("Player/m4");
                m5 = manager.Load<Texture2D>("Player/m5");                
                mm0 = manager.Load<Texture2D>("Player/mm0");
                mm1 = manager.Load<Texture2D>("Player/mm1");
                mm2 = manager.Load<Texture2D>("Player/mm2");
                mm3 = manager.Load<Texture2D>("Player/mm3");
                mm4 = manager.Load<Texture2D>("Player/mm4");
                mm5 = manager.Load<Texture2D>("Player/mm5");
                disparo = manager.Load<Texture2D>("disparo");
                disparo2 = manager.Load<Texture2D>("disparo2");
                b25 = manager.Load<Texture2D>("b25");
                b50 = manager.Load<Texture2D>("b50");
                b100 = manager.Load<Texture2D>("b100");
                backtest = manager.Load<Texture2D>("backtest");
                
            }
            catch (Exception ex)
            {
                Log.WarnException("LoadTexture catch ", ex);
                return false;
            }
            return true;
        }
        public static bool LoadSound()
        {
            try
            {
                lanzar = manager.Load<SoundEffect>("Efectos/lanzar");
                explotar = manager.Load<SoundEffect>("Efectos/explotar");
            }
            catch (Exception ex)
            {
                Log.WarnException("LoadSound catch ", ex);
                return false;
            }
            return true;
        }
        public static bool LoadFonts()
        {
            try
            {
                arial = manager.Load<SpriteFont>("Fonts/arial");
                miramob = manager.Load<SpriteFont>("Fonts/miramob");
            }
            catch (Exception ex)
            {
                Log.WarnException("LoadFonts catch ", ex);
                return false;
            }
            return true;
        }

    }
}
