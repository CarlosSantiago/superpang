﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SuperPang
{
    class RemotePlayer
    {
        private Vector2 position;
        private Vector2 dimensions;

        private Vector2 nextPosition;

        private Dictionary<int, string> playerTextures = new Dictionary<int, string>();
        private int currentTextureKey;

        private int animationTimer;
        private int millisecondsPerAnimationFrame;
        private int timeSinceLastFrame;
        private int timeSinceLastMove;
        private int timeSinceLastShot;

        public bool shoot;
        public Vector2 lastLanzaPos;

        public RemotePlayer(Vector2 inPosition)
        {
            this.position = inPosition;
            playerTextures.Add(1, "mm0");
            playerTextures.Add(2, "mm1");
            playerTextures.Add(3, "mm2");
            playerTextures.Add(4, "mm3");
            playerTextures.Add(-1, "m0");
            playerTextures.Add(-2, "m1");
            playerTextures.Add(-3, "m2");
            playerTextures.Add(-4, "m3");
            playerTextures.Add(5, "mm4");
            playerTextures.Add(-5, "m4");
            playerTextures.Add(6, "mm5");
            playerTextures.Add(-6, "m5");         
            dimensions.X = 60;
            dimensions.Y = 78;
            currentTextureKey = 5;
            millisecondsPerAnimationFrame = 100;
            timeSinceLastShot = 1000;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Texture2D texture = (Texture2D)typeof(MyContentManager).GetField(playerTextures[currentTextureKey]).GetValue(typeof(MyContentManager));
            spriteBatch.Draw(texture, position, Color.White);
        }

        public void Update(GameTime gameTime)
        {
            timeSinceLastFrame = gameTime.ElapsedGameTime.Milliseconds;
            animationTimer += timeSinceLastFrame;
            timeSinceLastMove += timeSinceLastFrame;
            timeSinceLastShot += timeSinceLastFrame;

            int distanceX = (int)nextPosition.X - (int)position.X;
            if (distanceX > 0)
            {
                timeSinceLastMove = 0;
                if (distanceX >= 5)
                    position.X += (int)(0.35f * timeSinceLastFrame);
                else
                    position.X -= (int)(0.35f * timeSinceLastFrame);

                if (animationTimer > millisecondsPerAnimationFrame)
                {
                    if (currentTextureKey < 0)
                    {
                        currentTextureKey = 1;
                    }
                    else if (currentTextureKey > 3)
                    {
                        currentTextureKey = 1;
                    }
                    else
                    {
                        currentTextureKey++;
                    }
                    animationTimer = 0;
                }

            }
            else if (distanceX < 0)
            {
                timeSinceLastMove = 0;
                if (distanceX <= -5)
                    position.X -= 5;
                else
                    position.X += distanceX;

                if (animationTimer > millisecondsPerAnimationFrame)
                {
                    if (currentTextureKey > 0)
                    {
                        currentTextureKey = -1;
                    }
                    else if (currentTextureKey < -3)
                    {
                        currentTextureKey = -1;
                    }
                    else
                    {
                        currentTextureKey--;
                    }
                    animationTimer = 0;
                }
            }
            else if (timeSinceLastMove > 40) 
            {
                if (currentTextureKey > 0)
                {
                    currentTextureKey = 5;
                }
                else if (currentTextureKey < 0)
                {
                    currentTextureKey = -5;
                }
            }

            if (timeSinceLastShot < 100)
            {
                if (currentTextureKey > 0)
                {
                    currentTextureKey = 6;
                }
                if (currentTextureKey < 0)
                {
                    currentTextureKey = -6;
                }
            }
            if (shoot)
            {
                timeSinceLastShot = 0;
                MyContentManager.lanzar.Play();
            }
        }

        public void setPosition(int x, int y)
        {
            this.nextPosition = new Vector2(x, y);
        }

    }
}
