﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SuperPang.Networking.SendPackets;
using SuperPang.Networking;

namespace SuperPang
{
    class MultiPlayState : IGameState
    {
        private UserInterface myInterface;
        private Player player;

        public static List<RemotePlayer> remotePlayers = new List<RemotePlayer>();

        public static List<GameObject> lanzas = new List<GameObject>();
        public static List<GameObject> bolas = new List<GameObject>();

        public enum MultiGameState { waiting, live };
        public MultiGameState currentState;

        private int localPlayerId;

        private int timeSinceLastPositionSend;
        private Vector2 lastPositionSent;

        public MultiPlayState(int localClientId, List<Vector2> playersPositions)
        {
            this.localPlayerId = localClientId;
            for (int i = 0; i < playersPositions.Count; i++)
            {
                if (i+1 == localPlayerId)
                {
                    this.player = new Player(playersPositions[i]);
                    continue;
                }
                remotePlayers.Add(new RemotePlayer(playersPositions[i]));
            }
            
        }

        public bool onEnter()
        {
            Log.Info("Entrando MultiPlayState");
            myInterface = new UserInterface();            
            currentState = MultiGameState.live;
            
            return true;
        }
        public bool onExit()
        {
            Log.Info("Saliendo MultiPlayState");
            return true;

        }
        public void Update(GameTime gameTime)
        { 
            if (currentState != MultiGameState.live)
                return;
            timeSinceLastPositionSend += gameTime.ElapsedGameTime.Milliseconds;
            player.Update(gameTime);

            if (player.canShoot)
            {
                new SendPShoot((int)player.getCurrentPosition().X, (int)player.getCurrentPosition().Y).Send(TcpClient.client);
                lanzas.Add(new Lanza(player.getCurrentPosition()));
                player.canShoot = false;
            }

            foreach (RemotePlayer r in remotePlayers)
            {
                r.Update(gameTime);
                if (r.shoot)
                {
                    lanzas.Add(new Lanza(r.lastLanzaPos));
                    r.shoot = false;
                }

            }

            if (player.getCurrentPosition().X != lastPositionSent.X)
            {
                if (timeSinceLastPositionSend > 120)
                {
                    timeSinceLastPositionSend = 0;
                }
                else if (timeSinceLastPositionSend > 100)
                {
                    timeSinceLastPositionSend = 0;
                    lastPositionSent = player.getCurrentPosition();
                    new SendPPlayerPos((int)player.getCurrentPosition().X, (int)player.getCurrentPosition().Y).Send(TcpClient.client);
                }
                
            }
            for (int x = 0; x < lanzas.Count; x++)
            {
                if (lanzas[x].getCurrentPosition().Y < 10)
                {
                    lanzas.RemoveAt(x);
                    break;
                }
                else
                    lanzas[x].Update(gameTime);
            }
            foreach (BouncingBall b in bolas)
            {
                b.Update(gameTime);
            }
            myInterface.Update(gameTime);
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            myInterface.Draw(spriteBatch);
            foreach (BouncingBall b in bolas)
            {
                b.Draw(spriteBatch);
            }
            foreach (Lanza l in lanzas)
            {
                l.Draw(spriteBatch);
            }
            player.Draw(spriteBatch);            
            foreach (RemotePlayer r in remotePlayers)
            {
                r.Draw(spriteBatch);
            }
        }
    }
}
