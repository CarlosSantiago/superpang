﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperPang.Networking.RecvPackets;
using SuperPang.Networking.SendPackets;

namespace SuperPang.Networking
{
    public class OpCodes
    {
        public static Dictionary<int, Type> Recv = new Dictionary<int, Type>();
        public static Dictionary<Type, int> Send = new Dictionary<Type, int>();

        public static void Init()
        {
            Recv.Add(1, typeof(RecvPHay));
            Recv.Add(3, typeof(RecvPLevelSetup));
            Recv.Add(4, typeof(RecvPPlayerMoved));
            Recv.Add(7, typeof(RecvPPlayerShoot));

            Send.Add(typeof(SendPJoinGame), 2);
            Send.Add(typeof(SendPPlayerPos), 5);
            Send.Add(typeof(SendPShoot), 6);

        }
    }
}
