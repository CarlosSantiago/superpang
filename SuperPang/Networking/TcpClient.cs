﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using SuperPang.Networking.RecvPackets;
using System.Net.NetworkInformation;
using System.Threading;


namespace SuperPang.Networking
{
    class TcpClient
    {
        public static Socket client;
        public static IPAddress direc;
        public static IPEndPoint ipep;

        public TcpClient(string ipAdress, int port)
        {
            direc = IPAddress.Parse(ipAdress);
            ipep = new IPEndPoint(direc, port);

            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public bool init()
        {
            try
            {
                client.Connect(ipep);
            }
            catch (SocketException ex)
            {                
                Log.ErrorException("Error al conectar con el servidor ", ex);
                return false;
            }

            ThreadStart ThListenerS = delegate { listen(); };
            Thread ThListener = new Thread(ThListenerS);
            ThListener.Start();
            return true;
        }

        private void listen()
        {
            bool listening = true;

            while (listening)
            {               
                byte[] msg = new byte[80];
                try
                {
                    client.Receive(msg);
                    string dirtyTrama = Encoding.ASCII.GetString(msg);                    
                    string clean = dirtyTrama.Trim('\0');
                    string[] tramas = clean.Split('%');
                    //Log.Info("Trama: " + clean);
                    foreach (string trama in tramas)
                    {
                        if (trama.Length > 0)
                        {
                            GameMessage mensaje = new GameMessage(trama);

                            ((IRecvPacket)Activator.CreateInstance(OpCodes.Recv[mensaje.getOpCode()])).Process(mensaje.getData());
                        }
                        else
                        {
                            break;
                        }  
                       
                    } 
                }
                catch (SocketException ex)
                {
                    Log.ErrorException("SocketException", ex);
                    listening = false;
                }
                catch (Exception ex) 
                {
                    Log.ErrorException("Exception en el socket: ", ex);
                    listening = false;
                }
            }
        }

        public static int getMs()
        {
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();
            options.DontFragment = true;

            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 120;
            PingReply reply = pingSender.Send(direc, timeout, buffer, options);
            if (reply.Status == IPStatus.Success)
            {
                return Convert.ToInt32(reply.RoundtripTime);
            }
            return -1;
        }
    }
}
