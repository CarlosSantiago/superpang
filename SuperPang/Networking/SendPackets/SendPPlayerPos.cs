﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace SuperPang.Networking.SendPackets
{
    class SendPPlayerPos
    {
        int x, y;
        public SendPPlayerPos(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public void Send(Socket socket)
        {
            string asciiMsg = GameMessage.getTrama(OpCodes.Send[this.GetType()], new List<string>() { x.ToString() , y.ToString()});
            Log.Debug("Enviando trama PlayerPos: " + asciiMsg);
            byte[] msg = Encoding.ASCII.GetBytes(asciiMsg);
            socket.Send(msg);
        }
    }
}
