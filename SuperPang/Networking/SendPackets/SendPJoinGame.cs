﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace SuperPang.Networking.SendPackets
{
    public class SendPJoinGame : ISendPacket
    {
        private int gameID, maxPlayers;
        private string username;
        public SendPJoinGame(int gameID, string username, int maxPlayers)
        {
            this.gameID = gameID;
            this.username = username;
            this.maxPlayers = maxPlayers;
        }
        public void Send(Socket socket)
        {
            string asciiMsg = GameMessage.getTrama(OpCodes.Send[this.GetType()], new List<string>() { gameID.ToString(), username, maxPlayers.ToString() });
            Log.Debug("Enviando trama JoinGame: " + asciiMsg);
            byte[] msg = Encoding.ASCII.GetBytes(asciiMsg);
            socket.Send(msg);
        }
    }
}
