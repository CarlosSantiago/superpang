﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuperPang.Networking;
using System.Net.Sockets;

namespace SuperPang.Networking.SendPackets
{
    interface ISendPacket
    {
        void Send(Socket socket);
    }
}
