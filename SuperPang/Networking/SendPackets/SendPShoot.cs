﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace SuperPang.Networking.SendPackets
{
    class SendPShoot : ISendPacket
    {
        int x, y;
        public SendPShoot(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public void Send(Socket socket)
        {
            string asciiMsg = GameMessage.getTrama(OpCodes.Send[this.GetType()], new List<string>() { x.ToString() , y.ToString()});
            Log.Debug("Enviando trama Shoot: " + asciiMsg);
            byte[] msg = Encoding.ASCII.GetBytes(asciiMsg);
            socket.Send(msg);
        }
    }
}
