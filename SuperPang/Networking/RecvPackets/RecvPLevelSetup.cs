﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SuperPang.Networking.RecvPackets
{
    class RecvPLevelSetup : IRecvPacket
    {
        public override void Process(List<string> data)
        {
            Log.Debug("El servidor envia el setup de nivel, miId: "+data[0]+" jugadores: "+data[1]);
            List<Vector2> posList = new List<Vector2>();
            int idLocalPlayer;
            try
            {
                idLocalPlayer = Convert.ToInt16(data[0]);             
                for (int i = 0; i < Convert.ToInt16(data[1])*2; i += 2)
                {
                    posList.Add(new Vector2(Convert.ToInt16(data[i + 2]), Convert.ToInt16(data[i + 3])));
                }
                GameStateMachine.pushState(new MultiPlayState(idLocalPlayer, posList));
            }
            catch
            {
                Log.Error("Error en el formato de trama de RecvPLevelSetup");
            }
            
        }
    }
}
