﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPang.Networking.RecvPackets
{
    class RecvPPlayerMoved : IRecvPacket
    {
        public override void Process(List<string> data)
        {           
            try
            {                
                for (int i = 0; i < (data.Count-1)/2; i++)
                {
                    MultiPlayState.remotePlayers[i].setPosition(Convert.ToInt32(data[(i*2)+1]), Convert.ToInt32(data[(i*2)+2]));
                    Log.Debug("RemotePlayer: " + i + " newPos: x" + data[(i * 2) + 1] + " y" + data[(i * 2) + 2]);
                }             
            }
            catch
            {
                Log.Error("Error en el formato de trama de RecvPWorldState");
            }
            
        }
    }
}
