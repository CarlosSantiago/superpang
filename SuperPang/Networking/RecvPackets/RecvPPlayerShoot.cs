﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SuperPang.Networking.RecvPackets
{
    class RecvPPlayerShoot : IRecvPacket
    {
        public override void Process(List<string> data)
        {
            try
            {
                Log.Debug("RevPPlayerShoot: " + data[0] + " " + data[1] + " " + data[2]);
                MultiPlayState.remotePlayers[0].lastLanzaPos = new Vector2(Convert.ToInt32(data[1]),Convert.ToInt32(data[2]));
                MultiPlayState.remotePlayers[0].shoot = true;  
                //INDICES DE REMOTEPLAERS PARA CASO DE 2 JUGADORES
            }
            catch
            {
                Log.Error("Error en el formato de trama de RecvPPlayerShoot");
            }

        }
    }
}
