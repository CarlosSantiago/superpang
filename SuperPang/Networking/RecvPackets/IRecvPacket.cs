﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace SuperPang.Networking.RecvPackets
{
    public abstract class IRecvPacket
    {
        public abstract void Process(List<string> data);       
    }
}
