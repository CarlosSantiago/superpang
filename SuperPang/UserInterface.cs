﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SuperPang.Networking;

namespace SuperPang
{
    class UserInterface
    {
        float fps;
        int ms;

        int timeSinceLastFrame;
        int timeSinceLastPingUpdate;        
        
        public UserInterface()
        {
            
        }

        public void Update(GameTime gameTime)
        {
            timeSinceLastFrame = gameTime.ElapsedGameTime.Milliseconds;
            timeSinceLastPingUpdate += timeSinceLastFrame;
            //fps = 1/(timeSinceLastFrame/1000f);
            //if (timeSinceLastPingUpdate >= 2000)
            //{
            //    timeSinceLastPingUpdate = 0;
            //    ms = TcpClient.getMs();
            //}

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(MyContentManager.backtest, new Vector2(0, -68), Color.White);
            spriteBatch.Draw(MyContentManager.bordes1, new Vector2(0, 0), Color.White);
            //spriteBatch.DrawString(MyContentManager.miramob, "FPS: "+fps.ToString(), new Vector2(30, 30), Color.White);
            spriteBatch.DrawString(MyContentManager.miramob, "Ping: "+ms.ToString()+"ms", new Vector2(30, 45), Color.White);
            //spriteBatch.DrawString(MyContentManager.miramob, "BB x" + MultiPlayState.bolas[0].getCurrentPosition().X + " y" + MultiPlayState.bolas[0].getCurrentPosition().Y, new Vector2(30, 65), Color.White);
        }

    }
}
